# Changelog

<a name="1.0.0"></a>
## 1.0.0 (2021-03-26)

### Added

- 👷‍♂️ fix: Changes on yml file [[c5c4228](https://gitlab.com/SClemenceau/pics/commit/c5c4228081279521cd92b17789c21ac248b7de04)]
- 👷‍♂️ fix: Changes on yml file [[1b8f756](https://gitlab.com/SClemenceau/pics/commit/1b8f7565d110d83ff4f58fc636057df38f2a169d)]
- 👷‍♂️ fix: Fix Deployment by adding npm [[fbc6be1](https://gitlab.com/SClemenceau/pics/commit/fbc6be10e28782edd002a2554a7bb030ea6a8e58)]
- 👷‍♂️ fix: Fix deployment by adding ruby image [[4fb90ef](https://gitlab.com/SClemenceau/pics/commit/4fb90efeeaa3953525c3072d2beed1a915edf559)]
- 👷‍♂️ feat: Ajout de déploiement automatique sur Heroku [[1f4c8c8](https://gitlab.com/SClemenceau/pics/commit/1f4c8c8e41b14ec28e79b586db86ba1a3abc5ef2)]
- 🎉 Initial Commit [[a0f04a1](https://gitlab.com/SClemenceau/pics/commit/a0f04a16d43c48c0ced9a2b8ee0b2b305bced3b4)]

### Miscellaneous

-  Merge branch &#x27;release/v1.0.0&#x27; [[db3629d](https://gitlab.com/SClemenceau/pics/commit/db3629dbaa9577f1c16888ac28355ca6327ce0b9)]
- 📝 docs: Mise à jour du ReadME [[0babe8e](https://gitlab.com/SClemenceau/pics/commit/0babe8ee7403e32e3f30ac6e365075e8bc9a8e6e)]
- 📝 docs: Ajout d&#x27;un changelog [[fa347df](https://gitlab.com/SClemenceau/pics/commit/fa347dfeb8de6abc349cdb8837660e78269764f6)]
- 🛂 chore: Changement de la clé d&#x27;API unsplash [[5af3189](https://gitlab.com/SClemenceau/pics/commit/5af31891f3c81e22285d95b8f8ff42e0566dbb24)]


