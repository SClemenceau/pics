import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID 81P2wl1vryGzHt_L6ssADLY2i8rW1c2lISxcdu9pWPw'
  }
});
